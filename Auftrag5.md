# Autrag 5

## Was wird unter einem Problemraum und Lösungsraum verstanden?
Problemraum, Struktur und Verhalten:

- richtige Frage finden, Feld festlegen

- Feld erforschen

- künstliche Perspektive einnehmen 

Lösungsraum:
- Ideen sammeln

- Idee auswählen, Prototyp herstellen

Problemraum 2:
- Prototyp erproben

- Produkt herstellen
  
## Welche Vorteile bringt eine Trennung zwischen Problemraum und Lösungsraum?
Explizite Entscheidungen mit besseren Lösungen
Freiere Suche nach optimalen Lösungen

## Was wird unter Requirements Engineering verstanden?
Requirements Engineering ist der Prozess der Definition, Dokumentation und Pflege von Anforderungen im technischen Entwurfsprozess. 
Es geht um anwendungsorientierte Analysetechniken, Modelle des Problemraums sowie das Prüfen anhand Feedback durch Prototypen.



## Was wird unter Modellen des Problemraumes verstanden? Geben Sie Beispiele
Modelle des Problemraums sollen alle Aspekte des Problemraums darstellen, die durch Anwendungssystem unterstützt werden sollen. Ein Beispiel ist ein Strukturdiagramm.



